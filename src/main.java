public class main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Buffer buffer=new Buffer();
		String alfabet="abc";
		int pass_length=alfabet.length();
		Producent producent = new Producent(buffer,alfabet,pass_length); //object Producent::producent
		Thread t_producent=new Thread(producent);		//Thread creation
		t_producent.start();							//start Thread
		Konsument konsument = new Konsument("Pierwszy ",pass_length,buffer); //object Konsument::konsument
		Thread t_konsument=new Thread(konsument);
		t_konsument.start();
		while(konsument.czy_zlamane_haslo==0); // vain
		t_producent.stop();
		t_konsument.stop();
		if (konsument.czy_zlamane_haslo==1)
		System.out.println("Podane przez producenta haslo jest zgodne z haslem konsumenta!");
		else if (konsument.czy_zlamane_haslo==2)
		{
			System.out.println("Producent nie wyprodukuje wiecej hasel! Konczy prace!");
		}
	}
}
