import java.util.ArrayList;
import java.util.List;

public class Buffer {
	static final int MAXQUEUE = 5; //ustawienie max dlugosci bufora
	List wiadomosci; //bufor na hasla
	Buffer()
	{
		this.wiadomosci= new ArrayList();
	}
	//jesli bufor jest zapelniony, to czekaj
	//w przeciwnym przypadku dogeneruj hasla
	synchronized void putMessage(Producent p ) 
	{
		while ( wiadomosci.size() >= MAXQUEUE )
			try 
				{
					wait();
				}
		catch(InterruptedException e) { }
		
		p.password_generator();
		notify();
	}
	
	// called by Konsument
	public synchronized String getMessage() 
	{
		while (wiadomosci.size() == 0) //jesli nie ma wiadomosci, daj sygnal innym watkom, czekaj
			try 
			{
				notify();
				wait();
			}
		catch(InterruptedException e) { }
		
		String message = (String)wiadomosci.remove(0); //pobierz wiadomosc i usun ja z listy
		notify();
		return message;
	}
}
