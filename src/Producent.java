public class Producent implements Runnable{
	String alfabet;
	StringBuffer haslo_buffer;
	int i=0;
	Boolean password_yes_or_not;
	int pass_length;
	int nr_hasla;
	int ktore_wyslane;
	//Obiekt bufor
	Buffer buffer;
	//KONSTRUKTOR
	Producent(Buffer buffer,String alfabet,int pass_length){
		this.alfabet= alfabet;
		this.haslo_buffer=new StringBuffer();
		this.password_yes_or_not=false;
		this.pass_length=pass_length;
		this.nr_hasla=0;
		this.ktore_wyslane=0;
		this.buffer=buffer;
	}
	public void run() //generuje hasla
	{	
		haslo_buffer.setLength(pass_length); //ustawia dlugosc hasla
		while (true) 
		{
			buffer.putMessage(this);
			try {
				
				Thread.sleep(1000);
				}
			catch (InterruptedException e) { }
		}
	}
	void password_generator(){
		if(ktore_wyslane<Math.pow(alfabet.length(), pass_length))
		{
			password_yes_or_not=false;
			nr_hasla=0;
			haslogen(pass_length, alfabet.length(), 0, i);
			i+=1;
			buffer.wiadomosci.add(haslo_buffer.toString());
			ktore_wyslane+=1;
		}
		else {buffer.wiadomosci.add("END");}
	}
	void haslogen(int n, int L, int level, int ktore) 
	{
	    if (level == n) { 
	    	if(ktore==nr_hasla)
    		{
	    		password_yes_or_not=true;
	    		return;
    		}
	    	nr_hasla+=1;
	    	}
	    else { 
	      for (int i=0;i<L;i++) 
	      {
	    	  haslo_buffer.setCharAt(level, alfabet.charAt(i));
	          haslogen(n,L,level+1,ktore);
	          if(password_yes_or_not==true) return;
	      }
	    }
	}
}