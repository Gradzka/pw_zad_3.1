import java.util.Random;

public class Konsument implements Runnable {
	String nazwa;
	String haslo;
	String alfabet;
	int pass_length;
	volatile Integer czy_zlamane_haslo;
	Buffer buffer;
	Konsument(String nazwa,int passlength, Buffer buffer) 
		{
			this.nazwa = nazwa;
			this.alfabet="abc";
			this.haslo=haslogen1(passlength, this.alfabet);
			//this.haslo="abb";
			this.czy_zlamane_haslo=0;
			this.pass_length=passlength;
			this.buffer=buffer;
		}
	
	
	public void run()
	{
	System.out.println("Has�o: " + haslo);
	String message;
	int i=0;
		while (true)
		{
			i++;
			message= buffer.getMessage();
			
			System.out.println("Otrzymalem has�o "+i+".: " + message);
			if(message.equals(haslo))
			{
				czy_zlamane_haslo=1;
				//System.out.println(czy_zlamane_haslo);
				
			}
			else if(message.equals("END"))
			{
				czy_zlamane_haslo=2;
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e) { }
		}
	}
	//Haslo generowane przez konsumenta
	public String haslogen1(int n, String alfabet) 
	{
		String haslo="";
	    Random rand=new Random();
	    for(int i=0;i<n;i++)
	    {
	    	haslo=haslo+alfabet.charAt(rand.nextInt(alfabet.length()));
	    }
	    return haslo;
	}
}